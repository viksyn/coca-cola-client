import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: ['./issue.component.css']
})
export class IssueComponent implements OnInit {
  @Output()
  emitIssue = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  submitIssue(name: string, email: string, issue: string) {
    console.log('test');

    this.emitIssue.emit({ name, email, issue });
  }
}
