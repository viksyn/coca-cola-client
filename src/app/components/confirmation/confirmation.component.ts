import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { AdminRoutingModule } from 'src/app/features/admin/admin-routing.module';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit {

  @Input()
  recordId: string;

  @Output()
  emitRecordId = new EventEmitter<string>();

  @Output()
  emitAdminId = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  getRecordId(id: string) {
    this.emitRecordId.emit(id);
  }

}
