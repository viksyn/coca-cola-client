import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
// TODO add the component to a module
@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {
  role = '';
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.authService.roles$.subscribe(role => {
      if (role) {
        this.role = role[0];
      }
    });
  }

  homeNavigate(role: string) {
    // const role = this.authService.role;
    if (role === 'Admin') {
      this.router.navigate(['admin']);
    } else {
      this.router.navigate(['user']);
    }
  }
}
