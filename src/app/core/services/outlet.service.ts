import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Outlet } from 'src/app/common/interfaces/outlet';

@Injectable({
  providedIn: 'root'
})
export class OutletService {
  constructor(private readonly http: HttpClient) {}

  getCustomerOutlets(customerId: string): Observable<Outlet[]> {
    return this.http.get<Outlet[]>(
      `http://localhost:3000/customer/${customerId}/outlets`
    );
  }

  getSingleOutlet(customerId: string, outletId: string): Observable<Outlet> {
    return this.http.get<Outlet>(
      `http://localhost:3000/customer/${customerId}/outlet/${outletId}`
    );
  }

  createOutlet(customerId: string, location: string): Observable<Outlet> {
    return this.http.post<Outlet>(
      `http://localhost:3000/customer/${customerId}/outlet/`,
      {
        location
      }
    );
  }

  editOutlet(
    customerId: string,
    outletId: string,
    newLocation: string
  ): Observable<Outlet> {
    return this.http.put<Outlet>(
      `http://localhost:3000/customer/${customerId}/outlet/${outletId}`,
      {
        location: newLocation
      }
    );
  }

  deleteOutlet(customerId: string, outletId: string) {
    return this.http.delete<Outlet>(
      `http://localhost:3000/customer/${customerId}/outlet/${outletId}`
    );
  }
}
