import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RegisterUser } from 'src/app/common/interfaces/register-user';
import { User } from 'src/app/common/interfaces/user';
import { RedemptionRecord } from 'src/app/common/interfaces/redemption-record';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private readonly http: HttpClient) { }

  getAllBasicUsers(): Observable<User[]> {
    return this.http.get<User[]>(`http://localhost:3000/basic-user`);
  }

  registerBasicUser(
    customerId: string,
    outletId: string,
    user: RegisterUser
  ): Observable<RegisterUser> {
    return this.http.post<RegisterUser>(
      `http://localhost:3000/customer/${customerId}/outlet/${outletId}/user`,
      user
    );
  }

  getCurrentUser(userId: string): Observable<User> {
    return this.http.get<User>(`http://localhost:3000/user/${userId}`);
  }

  changeUserOutlet(
    customerId: string,
    outletId: string,
    userId: string,
    newOutletId: string
  ): Observable<User> {
    return this.http.put<User>(
      `http://localhost:3000/customer/${customerId}/outlet/${outletId}/user/${userId}`,
      { outletId: newOutletId }
    );
  }

  deleteUser(customerId: string, outletId: string, userId: string) {
    return this.http.delete(
      `http://localhost:3000/customer/${customerId}/outlet/${outletId}/user/${userId}`
    );
  }

  getAllUsersForOutlet(
    customerId: string,
    outletId: string
  ): Observable<User[]> {
    return this.http.get<User[]>(
      `http://localhost:3000/customer/${customerId}/outlet/${outletId}/users`
    );
  }

  getUsersRedemptionRecord(): Observable<RedemptionRecord[]> {
    return this.http.get<RedemptionRecord[]>(`http://localhost:3000/outlet-record`);
  }

}
