import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { StorageService } from './storage.service';

describe('AuthService', () => {
  const http = jasmine.createSpyObj('HttpClient', ['get', 'post']);
  const storage = jasmine.createSpyObj('StorageService', [
    'set',
    'get',
    'remove',
    'clear'
  ]);

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: http
        },
        {
          provide: StorageService,
          useValue: storage
        }
      ]
    })
  );

  it('AuthService should be initialized', () => {
    // Arrange
    const service: AuthService = TestBed.get(AuthService);

    // Act & Assert
    expect(service).toBeTruthy();
  });

  it('logout should change the subject to null', () => {
    const service: AuthService = TestBed.get(AuthService);

    service.logout();

    service.user$.subscribe(username => expect(username).toBe(null));
  });

  it('logout should remove data from local storage - remove is called 4 times', () => {
    const service: AuthService = TestBed.get(AuthService);
    storage.remove.calls.reset();
    service.logout();

    expect(storage.remove).toHaveBeenCalledTimes(4);
  });

  it('logout should remove user data from the local storage - remove is called with token, username, userID, roles', () => {
    const service: AuthService = TestBed.get(AuthService);
    storage.remove.calls.reset();
    service.logout();

    expect(storage.remove).toHaveBeenCalledWith('token');
    expect(storage.remove).toHaveBeenCalledWith('username');
    expect(storage.remove).toHaveBeenCalledWith('userID');
    expect(storage.remove).toHaveBeenCalledWith('roles');
  });

  // it('register method should have called http.post once', () => {
  //     const service: AuthService = TestBed.get(AuthService);
  //     // http.post.calls.reset();
  //     const user = {
  //         name: 'username',
  //         email: 'email@email.com',
  //         password: 'Hello123!'
  //     }

  //     service.register(user).subscribe(
  //         () => {
  //             expect(http.post).toHaveBeenCalledTimes(1)
  //         }
  //     );
  // });
});
