import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TokenUser } from 'src/app/common/interfaces/token-user';
import { JwtService } from './jwt.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly userSubject$ = new BehaviorSubject<string | null>(
    this.username
  );
  private readonly userRoleSubject$ = new BehaviorSubject<string[] | null>(
    this.loggedUserRoles()
  );
  private readonly jwtTokenSubject$ = new BehaviorSubject<{
    token: string;
  } | null>(null);

  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly jwt: JwtService
  ) {}

  public get user$() {
    return this.userSubject$.asObservable();
  }

  private loggedUserRoles(): string[] | null {
    const token = this.storage.get('token');
    if (!token) {
      return null;
    }
    const decodedToken = this.jwt.decode(token);
    const roles = decodedToken.roles.map(role => role.name);

    return roles;
  }

  public get jwtToken$() {
    return this.jwtTokenSubject$.asObservable();
  }

  public get roles$() {
    return this.userRoleSubject$.asObservable();
  }

  private get username(): string | null {
    const token = this.storage.get('token');
    const username = this.storage.get('username') || '';
    if (token) {
      return username;
    }
    return null;
  }

  public login(user): Observable<any> {
    return this.http.post('http://localhost:3000/login', user).pipe(
      // tap((response: any) => {

      // }),
      map((response: any) => {
        const decodedToken: TokenUser = this.jwt.decode(response.token);
        const roles = decodedToken.roles.map(role => role.name);

        this.storage.set('token', response.token);
        this.storage.set('username', decodedToken.name);
        this.storage.set('userId', decodedToken.id);

        this.userRoleSubject$.next(roles);
        this.userSubject$.next(decodedToken.name);
        this.jwtTokenSubject$.next(response.token);

        return {
          username: decodedToken.name,
          roles
        };
      })
    );
  }

  public logout(): void {
    this.userSubject$.next(null);
    this.userRoleSubject$.next(null);
    this.jwtTokenSubject$.next(null);
    this.storage.remove('token');
    this.storage.remove('username');
    this.storage.remove('userId');
  }
}
