import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Admin } from 'src/app/common/interfaces/admin';
import { RegisterUser } from 'src/app/common/interfaces/register-user';


@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private readonly http: HttpClient) {}

  getAllAdmins(): Observable<Admin[]> {
    return this.http.get<Admin[]>(`http://localhost:3000/users`);
  }

  registerAdmin(admin: RegisterUser) {
    return this.http.post('http://localhost:3000/user', admin);
  }

  deleteAdmin(id: string) {
    return this.http.delete(`http://localhost:3000/user/${id}`);
  }

}
