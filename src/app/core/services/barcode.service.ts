import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BarcodeService {
  constructor(private readonly http: HttpClient) {}

  submitBarcode(barcode: string): Observable<{ message: string }> {
    return this.http.post<{ message: string }>(
      `http://localhost:3000/barcode`,
      {
        barcode
      }
    );
  }

  redeemPrize(barcode: string, prizeBarcode: string) {
    const barcodeObj = { barcode, prizeBarcode };
    return this.http.post<{ message: string }>(
      `http://localhost:3000/barcode/redeem`,
      barcodeObj
    );
  }

  reportMultipleRedemptionAttempt(barcode: string) {
    console.log(barcode);

    return this.http.post(`http://localhost:3000/barcode/report`, {
      barcode
    });
  }

  submitIssue(issueObj: { name: string; email: string; issue: string }) {
    console.log(issueObj);

    return this.http.post(`http://localhost:3000/submit-issue`, issueObj);
  }
}
