import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Activity } from 'src/app/common/interfaces/activity';
import { RedemptionRecord } from 'src/app/common/interfaces/redemption-record';

@Injectable({
  providedIn: 'root'
})
export class RedemptionRecordService {
  constructor(private readonly http: HttpClient) { }

  getAllRedemptionRecord(): Observable<RedemptionRecord[]> {
    return this.http.get<RedemptionRecord[]>(
      `http://localhost:3000/redemption-record/`
    );
  }

  getActivity(): Observable<Activity[]> {
    return this.http.get<Activity[]>(`http://localhost:3000/activity-log/`);
  }

}
