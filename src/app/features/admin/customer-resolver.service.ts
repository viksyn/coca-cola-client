import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Customer } from 'src/app/common/interfaces/customer';
import { CustomerService } from 'src/app/core/services/customer.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerResolveService implements Resolve<Customer[] | {}> {
  constructor(private readonly customerService: CustomerService) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.customerService.getAllCustomers().pipe(
      catchError(res => {
        return of({ all_customers: [] });
      })
    );
  }
}
