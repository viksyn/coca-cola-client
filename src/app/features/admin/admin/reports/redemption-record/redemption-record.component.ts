import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, PipeTransform } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { RedemptionRecord } from 'src/app/common/interfaces/redemption-record';

@Component({
  selector: 'app-redemption-record',
  templateUrl: './redemption-record.component.html',
  styleUrls: ['./redemption-record.component.css'],
  providers: [DatePipe]
})
export class RedemptionRecordComponent implements OnInit {
  @Input()
  redemptionRecords: RedemptionRecord[];

  records$: Observable<RedemptionRecord[]>;
  filter = new FormControl('');
  private p = 1;

  constructor(private readonly pipe: DatePipe) { }

  ngOnInit() {
    this.records$ = this.filter.valueChanges.pipe(
      startWith(''),
      map(text => this.search(text, this.pipe))
    );
  }

  search(text: string, pipe: PipeTransform): RedemptionRecord[] {
    return this.redemptionRecords.filter(record => {
      const term = text.toLowerCase();

      return (
        record.brand.brand.toLowerCase().includes(term) ||
        record.outlet.location.toLowerCase().includes(term) ||
        record.prize.prize.toLowerCase().includes(term) ||
        record.user.userName.toLowerCase().includes(term) ||
        pipe
          .transform(record.timestamp)
          .toLowerCase()
          .includes(term)
      );
    });
  }
}
