import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedemptionRecordComponent } from './redemption-record.component';

describe('RedemptionRecordComponent', () => {
  let component: RedemptionRecordComponent;
  let fixture: ComponentFixture<RedemptionRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedemptionRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedemptionRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
