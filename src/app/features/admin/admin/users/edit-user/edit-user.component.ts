import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Outlet } from 'src/app/common/interfaces/outlet';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  @Input()
  allOutlets: Outlet[];

  newOutlet: string;

  @Output()
  emitNewOutletName = new EventEmitter<string>();

  constructor() { }

  ngOnInit() { }

  getNewOutletName(outletLocation: string) {
    this.newOutlet = outletLocation;
  }

  emitOutletName(outletName: string) {
    outletName = this.newOutlet;
    this.emitNewOutletName.emit(outletName);
  }
}
