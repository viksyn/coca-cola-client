import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Customer } from 'src/app/common/interfaces/customer';
import { Outlet } from 'src/app/common/interfaces/outlet';
import { RegisterUser } from 'src/app/common/interfaces/register-user';
import { User } from 'src/app/common/interfaces/user';

@Component({
  selector: 'app-display-outlet-users',
  templateUrl: './display-outlet-users.component.html',
  styleUrls: ['./display-outlet-users.component.css']
})
export class DisplayOutletUsersComponent implements OnInit {
  @Input()
  customer: Customer;

  @Input()
  currentOutlet: Outlet;

  @Input()
  customerOutlets: Outlet[];

  @Input()
  users: User[];

  @Input()
  currentCustomerBrand = '';

  @Input()
  currentOutletLocation = '';

  @Output()
  emitUserId = new EventEmitter<string>();

  @Output()
  emitRegisterUser = new EventEmitter<RegisterUser>();

  @Output()
  emitDeleteUserId = new EventEmitter<string>();

  private p = 1;

  constructor() { }

  ngOnInit() { }

  registerUser(userToReg: RegisterUser) {
    this.emitRegisterUser.emit(userToReg);
  }

  getUserIdToEdit(userId: string) {
    this.emitUserId.emit(userId);
  }

  deleteUser(userId: string) {
    this.emitDeleteUserId.emit(userId);
  }
}
