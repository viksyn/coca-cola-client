import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageOutletUsersComponent } from './manage-outlet-users.component';

describe('ManageOutletUsersComponent', () => {
  let component: ManageOutletUsersComponent;
  let fixture: ComponentFixture<ManageOutletUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageOutletUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageOutletUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
