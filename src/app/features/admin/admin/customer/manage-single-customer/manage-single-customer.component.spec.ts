import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { Data, Params } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from 'src/app/components/confirmation/confirmation.component';
import { RegisterComponent } from 'src/app/components/register/register.component';
import { CustomerService } from 'src/app/core/services/customer.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { OutletService } from 'src/app/core/services/outlet.service';
import { ManageAdminComponent } from '../../../manage-all-users/manage-admin/manage-admin.component';
import { ManageAllUsersComponent } from '../../../manage-all-users/manage-all-users.component';
import { ManageUsersComponent } from '../../../manage-all-users/manage-users/manage-users.component';
import { AdminComponent } from '../../admin.component';
import { ManageOutletsComponent } from '../../outlets/manage-outlets/manage-outlets.component';
import { AuditTrailComponent } from '../../reports/audit-trail/audit-trail.component';
import { RedemptionRecordComponent } from '../../reports/redemption-record/redemption-record.component';
import { ReportsComponent } from '../../reports/reports.component';
import { DisplayOutletUsersComponent } from '../../users/display-outlet-users/display-outlet-users.component';
import { EditUserComponent } from '../../users/edit-user/edit-user.component';
import { ManageOutletUsersComponent } from '../../users/manage-outlet-users/manage-outlet-users.component';
import { CreateCustomerComponent } from '../create-customer/create-customer.component';
import { EditCustomerComponent } from '../edit-customer/edit-customer.component';
import { ManageCustomersComponent } from '../manage-customers/manage-customers.component';
import { ManageSingleCustomerComponent } from './manage-single-customer.component';

fdescribe('ManageSingleCustomerComponent', () => {
  let component: ManageSingleCustomerComponent;
  let fixture: ComponentFixture<ManageSingleCustomerComponent>;
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  const currentCustomer = {
    brand: 'billa',
    createdOn: '2019-07-25T21:10:26.341Z',
    id: 'c35037a0-e743-4b11-a3cf-8ff2b5c7c5db'
  };

  const outlets = [
    {
      location: 'smth',
      createdOn: '2019-07-27T18:35:45.888Z',
      id: 'bd0563ab-e5e4-49c2-b2a8-7b53afff9cbf'
    },
    {
      location: 'smth2',
      createdOn: '2019-07-27T18:35:45.888Z',
      id: 'c35037a0-e743-4b11-a3cf-8ff2b5c7c5kq'
    }
  ];

  const idArray = ['c35037a0-e743-4b11-a3cf-8ff2b5c7c5db'];

  const activatedRoute = {
    data: {
      subscribe: (fn: (value: Data) => void) =>
        fn({
          outlets
        })
    },
    params: {
      subscribe: (fn: (value: Params) => void) =>
        fn({
          idArray
        })
    }
  };

  const customerService = jasmine.createSpyObj('CustomerService', [
    'getAllCustomers',
    'getSingleCustomer',
    'createCustomer',
    'deleteCustomer'
  ]);

  const notificatorService = jasmine.createSpyObj('NotificatorService', [
    'success',
    'error'
  ]);

  const outletService = jasmine.createSpyObj('OutletService', [
    'getCustomerOutlets',
    'getSingleOutlet',
    'createOutlet',
    'editOutlet',
    'deleteOutlet'
  ]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgbModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterTestingModule
      ],
      declarations: [
        ManageCustomersComponent,
        AdminComponent,
        ManageSingleCustomerComponent,
        ManageAdminComponent,
        RegisterComponent,
        ManageUsersComponent,
        CreateCustomerComponent,
        EditCustomerComponent,
        ManageAllUsersComponent,
        ManageOutletsComponent,
        ReportsComponent,
        AuditTrailComponent,
        RedemptionRecordComponent,
        ManageOutletUsersComponent,
        DisplayOutletUsersComponent,
        EditUserComponent,
        ConfirmationComponent
      ],
      providers: [
        ManageCustomersComponent,
        {
          provide: CustomerService,
          useValue: customerService
        },
        {
          provide: OutletService,
          useValue: outletService
        },
        {
          provide: NotificatorService,
          useValue: notificatorService
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageSingleCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    fixture = TestBed.createComponent(ManageSingleCustomerComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should contain the information for all customer outlets', async () => {
    fixture = TestBed.createComponent(ManageSingleCustomerComponent);
    const app = fixture.debugElement.componentInstance;

    let outletData = [];

    activatedRoute.data.subscribe(data => {
      outletData = data.outlets;
    });

    expect(outletData).toContain(
      {
        location: 'smth',
        createdOn: '2019-07-27T18:35:45.888Z',
        id: 'bd0563ab-e5e4-49c2-b2a8-7b53afff9cbf'
      },
      {
        location: 'smth2',
        createdOn: '2019-07-27T18:35:45.888Z',
        id: 'c35037a0-e743-4b11-a3cf-8ff2b5c7c5kq'
      }
    );
  });

  it('should contain the id for the customer', async () => {
    fixture = TestBed.createComponent(ManageSingleCustomerComponent);
    const app = fixture.debugElement.componentInstance;

    let paramIds = [];

    activatedRoute.params.subscribe(params => {
      paramIds = params.idArray;
    });

    expect(paramIds).toContain('c35037a0-e743-4b11-a3cf-8ff2b5c7c5db');
  });
});
