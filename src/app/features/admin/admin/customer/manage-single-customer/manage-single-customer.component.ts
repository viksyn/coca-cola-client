import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Customer } from 'src/app/common/interfaces/customer';
import { Outlet } from 'src/app/common/interfaces/outlet';
import { CustomerService } from 'src/app/core/services/customer.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { OutletService } from 'src/app/core/services/outlet.service';

@Component({
  selector: 'app-manage-single-customer',
  templateUrl: './manage-single-customer.component.html',
  styleUrls: ['./manage-single-customer.component.css']
})
export class ManageSingleCustomerComponent implements OnInit {
  customerId = '';
  currentCustomer: Customer;
  currentCustomerBrand = '';
  outlets: Outlet[];

  constructor(
    readonly route: ActivatedRoute,
    private readonly customerService: CustomerService,
    private readonly outletService: OutletService,
    private readonly notificator: NotificatorService
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      params => {
        this.customerId = params.get('customerId');

        this.customerService.getSingleCustomer(this.customerId).subscribe(
          returnCustomer => {
            this.currentCustomer = returnCustomer;
            this.currentCustomerBrand = returnCustomer.brand;
          },
          error => {
            console.log(
              // tslint:disable-next-line:quotemark
              "there is an error in manage-single-customer.component's inner subscribe"
            );
          }
        );
      },
      error => {
        console.log('Error inside manage-single-customer.component.ts');
      }
    );

    this.route.data.subscribe(
      data => {
        this.outlets = data.all_customer_outlets;
      },
      error => {
        console.log(
          // tslint:disable-next-line:quotemark
          "there is an error in manage-single-customer.component's inner subscribe"
        );
      }
    );
  }

  createOutlet(location: string) {
    return this.outletService
      .createOutlet(this.currentCustomer.id, location)
      .subscribe(
        response => {
          this.notificator.success(`Outlet Created`);
          this.outlets.push(response);
        },
        error => {
          this.notificator.error('The name is incorrect! Please try again!');
        }
      );
  }

  editOutlet(editObj: { outletId: string; newLocation: string }) {

    return this.outletService
      .editOutlet(
        this.currentCustomer.id,
        editObj.outletId,
        editObj.newLocation
      )
      .subscribe(
        response => {
          this.outlets
            .filter(outlet => outlet.id === editObj.outletId)
            .map(outlet => (outlet.location = editObj.newLocation));
          this.notificator.success(`Outlet Edited`);
        },
        error => {
          this.notificator.error('There was an error! Please try again or contact our administrator!');
        }
      );
  }

  deleteOutlet(outletId: string) {
    return this.outletService
      .deleteOutlet(this.currentCustomer.id, outletId)
      .subscribe(
        response => {
          this.notificator.success(`Outlet Deleted`);
          const deletedOutlet = this.outlets.find(
            outlet => outlet.id === outletId
          );
          const outletIndex = this.outlets.indexOf(deletedOutlet);
          this.outlets.splice(outletIndex, 1);
        },
        error => {
          this.notificator.error('There was an error! Please try again or contact our administrator!');
        }
      );
  }
}
