import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Customer } from 'src/app/common/interfaces/customer';
import { CustomerService } from 'src/app/core/services/customer.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-manage-customers',
  templateUrl: './manage-customers.component.html',
  styleUrls: ['./manage-customers.component.css']
})
export class ManageCustomersComponent implements OnInit {

  customers: Customer[];
  isCollapsed = true;
  selectedCustomerId;
  private p = 1;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly customerService: CustomerService,
    private readonly notificator: NotificatorService
  ) { }

  ngOnInit() {
    this.route.data.subscribe(data => (this.customers = data.all_customers));
  }

  getCustomer(customerId: string) {
    this.selectedCustomerId = customerId;
  }

  createCustomer(brandName: string) {
    return this.customerService.createCustomer(brandName).subscribe(
      response => {
        this.notificator.success(`Customer Created`);
        this.customers.push(response);
      },
      error => {
        this.notificator.error('The name is incorrect, try with another one!');
      }
    );
  }

  editCustomer(newCustomerName: string) {
    return this.customerService
      .editCustomer(this.selectedCustomerId, newCustomerName)
      .subscribe(
        response => {
          this.customers
            .filter(customer => customer.id === this.selectedCustomerId)
            .map(customer => (customer.brand = newCustomerName));
          this.notificator.success(`Customer Edited`);
        },
        error => {
          this.notificator.error('There was an error! Please, try again or contact our administrator!');
        }
      );
  }

  deleteCustomer(customerId: string) {
    return this.customerService.deleteCustomer(customerId).subscribe(
      response => {
        this.notificator.success(`Customer Deleted`);
        const deletedCustomer = this.customers.find(
          customer => customer.id === customerId
        );
        const customerIndex = this.customers.indexOf(deletedCustomer);
        this.customers.splice(customerIndex, 1);
      },
      error => {
        this.notificator.error('There was an error! Please try again or contact our administrator!');
      }
    );
  }
}
