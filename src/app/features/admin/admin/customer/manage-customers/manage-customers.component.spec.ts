import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ActivatedRoute, Data, Router, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from 'src/app/components/confirmation/confirmation.component';
import { RegisterComponent } from 'src/app/components/register/register.component';
import { CustomerService } from 'src/app/core/services/customer.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { ManageAdminComponent } from '../../../manage-all-users/manage-admin/manage-admin.component';
import { ManageAllUsersComponent } from '../../../manage-all-users/manage-all-users.component';
import { ManageUsersComponent } from '../../../manage-all-users/manage-users/manage-users.component';
import { AdminComponent } from '../../admin.component';
import { ManageOutletsComponent } from '../../outlets/manage-outlets/manage-outlets.component';
import { AuditTrailComponent } from '../../reports/audit-trail/audit-trail.component';
import { RedemptionRecordComponent } from '../../reports/redemption-record/redemption-record.component';
import { ReportsComponent } from '../../reports/reports.component';
import { DisplayOutletUsersComponent } from '../../users/display-outlet-users/display-outlet-users.component';
import { EditUserComponent } from '../../users/edit-user/edit-user.component';
import { ManageOutletUsersComponent } from '../../users/manage-outlet-users/manage-outlet-users.component';
import { CreateCustomerComponent } from '../create-customer/create-customer.component';
import { EditCustomerComponent } from '../edit-customer/edit-customer.component';
import { ManageSingleCustomerComponent } from '../manage-single-customer/manage-single-customer.component';
import { ManageCustomersComponent } from './manage-customers.component';

fdescribe('ManageCustomersComponent', () => {
  let component: ManageCustomersComponent;
  let fixture: ComponentFixture<ManageCustomersComponent>;

  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

  const customers = [
    {
      brand: 'addidas',
      createdOn: '2019-07-27T18:35:45.888Z',
      id: 'bd0563ab-e5e4-49c2-b2a8-7b53afff9cbf'
    },
    {
      brand: 'billa',
      createdOn: '2019-07-25T21:10:26.341Z',
      id: 'c35037a0-e743-4b11-a3cf-8ff2b5c7c5db'
    }
  ];

  const activatedRoute = {
    data: {
      subscribe: (fn: (value: Data) => void) =>
        fn({
          customers
        })
    }
  };

  const customerService = jasmine.createSpyObj('CustomerService', [
    'getAllCustomers',
    'getSingleCustomer',
    'createCustomer',
    'deleteCustomer'
  ]);

  const notificatorService = jasmine.createSpyObj('NotificatorService', [
    'success',
    'error'
  ]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgbModule,
        RouterModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [
        ManageCustomersComponent,
        AdminComponent,
        ManageSingleCustomerComponent,
        ManageAdminComponent,
        RegisterComponent,
        ManageUsersComponent,
        CreateCustomerComponent,
        EditCustomerComponent,
        ManageAllUsersComponent,
        ManageOutletsComponent,
        ReportsComponent,
        AuditTrailComponent,
        RedemptionRecordComponent,
        ManageOutletUsersComponent,
        DisplayOutletUsersComponent,
        EditUserComponent,
        ConfirmationComponent
      ],
      providers: [
        ManageCustomersComponent,
        {
          provide: CustomerService,
          useValue: customerService
        },
        {
          provide: NotificatorService,
          useValue: notificatorService
        },
        {
          provide: ActivatedRoute,
          useValue: activatedRoute
        },
        {
          provide: Router,
          useValue: routerSpy
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    fixture = TestBed.createComponent(ManageCustomersComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should contain the information for all customers', async () => {
    fixture = TestBed.createComponent(ManageCustomersComponent);
    const app = fixture.debugElement.componentInstance;

    let customersData = [];

    activatedRoute.data.subscribe(data => {
      customersData = data.customers;
    });

    expect(customersData).toContain(
      {
        brand: 'addidas',
        createdOn: '2019-07-27T18:35:45.888Z',
        id: 'bd0563ab-e5e4-49c2-b2a8-7b53afff9cbf'
      },
      {
        brand: 'billa',
        createdOn: '2019-07-25T21:10:26.341Z',
        id: 'c35037a0-e743-4b11-a3cf-8ff2b5c7c5db'
      }
    );
  });

  it('should contain the right information for brand date and id', async () => {
    fixture = TestBed.createComponent(ManageCustomersComponent);
    const app = fixture.debugElement.componentInstance;

    let brand: string;
    let createdOn: string;
    let id: string;

    activatedRoute.data.subscribe(data => {
      brand = data.customers[0].brand;
      createdOn = data.customers[0].createdOn;
      id = data.customers[0].id;
    });

    expect(brand).toBe('addidas');
    expect(createdOn).toBe('2019-07-27T18:35:45.888Z');
    expect(id).toBe('bd0563ab-e5e4-49c2-b2a8-7b53afff9cbf');
  });

  it('should call delete customer', async () => {
    fixture = TestBed.createComponent(ManageCustomersComponent);
    const app = fixture.debugElement.componentInstance;

    const id = 'bd0563ab-e5e4-49c2-b2a8-7b53afff9cbf';

    spyOn(component, 'deleteCustomer');

    component.deleteCustomer(id);

    expect(component.deleteCustomer).toHaveBeenCalledTimes(1);
  });

  it('should call edit customer', async () => {
    fixture = TestBed.createComponent(ManageCustomersComponent);
    const app = fixture.debugElement.componentInstance;

    const newName = 'billa1';

    spyOn(component, 'editCustomer');

    component.editCustomer(newName);

    expect(component.editCustomer).toHaveBeenCalledTimes(1);
  });

  it('should call create customer', async () => {
    fixture = TestBed.createComponent(ManageCustomersComponent);
    const app = fixture.debugElement.componentInstance;

    const brandName = 'TMarket';

    spyOn(component, 'createCustomer');

    component.createCustomer(brandName);

    expect(component.createCustomer).toHaveBeenCalledTimes(1);
  });
});
