import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.css']
})
export class CreateCustomerComponent implements OnInit {
  @Output()
  emitItemName = new EventEmitter<string>();
  inputValue: string;

  constructor() {}

  ngOnInit() {}

  createItem(brandName: string) {
    this.emitItemName.emit(brandName);
    this.inputValue = '';
  }
}
