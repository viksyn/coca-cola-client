import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CustomerService } from 'src/app/core/services/customer.service';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.css']
})
export class EditCustomerComponent implements OnInit {
  @Output()
  emitEdittedName = new EventEmitter<{}>();

  constructor(
    readonly route: ActivatedRoute,
    private readonly customerService: CustomerService
  ) {}

  ngOnInit() {}

  editItem(newName: string) {
    this.emitEdittedName.emit(newName);
  }
}
