import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Outlet } from 'src/app/common/interfaces/outlet';

@Component({
  selector: 'app-manage-outlets',
  templateUrl: './manage-outlets.component.html',
  styleUrls: ['./manage-outlets.component.css']
})
export class ManageOutletsComponent implements OnInit {
  @Input()
  currentCustomerBrand: string;
  @Input()
  outlets: Outlet[] = [];
  selectedOutletId: string;
  @Output()
  emitNewLocation = new EventEmitter<{
    outletId: string;
    newLocation: string;
  }>();
  @Output()
  emitCreateOutlet = new EventEmitter<string>();
  @Output()
  emitDeleteOutlet = new EventEmitter<string>();

  private p = 1;

  constructor() { }

  ngOnInit() { }

  getOutlet(outletId: string) {
    this.selectedOutletId = outletId;
  }

  editOutlet(newLocation: string) {
    this.emitNewLocation.emit({ outletId: this.selectedOutletId, newLocation });
  }
  createOutlet(location: string) {
    this.emitCreateOutlet.emit(location);
  }
  deleteOutlet(outletId: string) {
    this.emitDeleteOutlet.emit(outletId);
  }
}
