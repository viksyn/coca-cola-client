import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageOutletsComponent } from './manage-outlets.component';

describe('ManageOutletsComponent', () => {
  let component: ManageOutletsComponent;
  let fixture: ComponentFixture<ManageOutletsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageOutletsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageOutletsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
