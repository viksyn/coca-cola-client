import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { User } from 'src/app/common/interfaces/user';
import { UserService } from 'src/app/core/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class UserResolverService implements Resolve<User[] | {}> {
  constructor(private readonly userService: UserService) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.userService.getAllBasicUsers().pipe(
      catchError(res => {
        return of({ users: [] });
      })
    );
  }
}
