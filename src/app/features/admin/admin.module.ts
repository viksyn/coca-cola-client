import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TokenInterceptorService } from 'src/app/auth/token-interceptor.service';
import { RegisterComponent } from 'src/app/components/register/register.component';
import { CoreModule } from 'src/app/core/core.module';
import { CustomerService } from 'src/app/core/services/customer.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin/admin.component';
import { CreateCustomerComponent } from './admin/customer/create-customer/create-customer.component';
import { EditCustomerComponent } from './admin/customer/edit-customer/edit-customer.component';
import { ManageCustomersComponent } from './admin/customer/manage-customers/manage-customers.component';
import { ManageSingleCustomerComponent } from './admin/customer/manage-single-customer/manage-single-customer.component';
import { ManageOutletsComponent } from './admin/outlets/manage-outlets/manage-outlets.component';
import { AuditTrailComponent } from './admin/reports/audit-trail/audit-trail.component';
import { RedemptionRecordComponent } from './admin/reports/redemption-record/redemption-record.component';
import { ReportsComponent } from './admin/reports/reports.component';
import { CustomerResolveService } from './customer-resolver.service';
import { ManageAdminComponent } from './manage-all-users/manage-admin/manage-admin.component';
import { ManageAllUsersComponent } from './manage-all-users/manage-all-users.component';
import { ManageUsersComponent } from './manage-all-users/manage-users/manage-users.component';
import { ManageOutletUsersComponent } from './admin/users/manage-outlet-users/manage-outlet-users.component';
import { DisplayOutletUsersComponent } from './admin/users/display-outlet-users/display-outlet-users.component';
import { EditUserComponent } from './admin/users/edit-user/edit-user.component';
import { ConfirmationComponent } from 'src/app/components/confirmation/confirmation.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    AdminComponent,
    ManageCustomersComponent,
    ManageSingleCustomerComponent,
    ManageAdminComponent,
    RegisterComponent,
    ManageUsersComponent,
    CreateCustomerComponent,
    EditCustomerComponent,
    ManageAllUsersComponent,
    ManageOutletsComponent,
    ReportsComponent,
    AuditTrailComponent,
    RedemptionRecordComponent,
    ManageOutletUsersComponent,
    DisplayOutletUsersComponent,
    EditUserComponent,
    ConfirmationComponent,
  ],
  imports: [CoreModule, AdminRoutingModule, NgbModule, SharedModule, NgxPaginationModule],
  providers: [
    CustomerService,
    CustomerResolveService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ]
})
export class AdminModule { }
