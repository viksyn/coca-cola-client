import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Activity } from 'src/app/common/interfaces/activity';
import { RedemptionRecordService } from 'src/app/core/services/redemption-record.service';

@Injectable({
  providedIn: 'root'
})
export class ActivityResolverService implements Resolve<Activity[] | {}> {
  constructor(
    private readonly redemtionRecordService: RedemptionRecordService
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.redemtionRecordService.getActivity().pipe(
      catchError(res => {
        return of([]);
      })
    );
  }
}
