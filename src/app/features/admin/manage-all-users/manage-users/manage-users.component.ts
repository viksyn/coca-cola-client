import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Customer } from 'src/app/common/interfaces/customer';
import { Outlet } from 'src/app/common/interfaces/outlet';
import { RegisterUser } from 'src/app/common/interfaces/register-user';
import { User } from 'src/app/common/interfaces/user';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.css']
})
export class ManageUsersComponent implements OnInit {
  @Input()
  customers: Customer[];

  @Input()
  outlets: Outlet[];

  @Input()
  users: User[];

  @Output()
  emitUserIdToDelete = new EventEmitter<string>();

  @Output()
  emitCustomer = new EventEmitter<string>();

  @Output()
  emitOutlet = new EventEmitter<string>();

  @Output()
  emitRegisterUser = new EventEmitter<RegisterUser>();

  disableOutletInput = true;

  private p = 1;

  constructor() { }

  ngOnInit() { }

  getCustomer(customer: string) {
    this.emitCustomer.emit(customer);
    this.disableOutletInput = false;
  }

  getOutlet(outlet: string) {
    this.emitOutlet.emit(outlet);
  }

  getUserIdToDelete(id: string) {
    this.emitUserIdToDelete.emit(id);
  }

  registerUser(regUser: RegisterUser) {
    this.emitRegisterUser.emit(regUser);
  }
}
