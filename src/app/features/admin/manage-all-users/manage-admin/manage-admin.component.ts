import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Admin } from 'src/app/common/interfaces/admin';
import { AdminService } from 'src/app/core/services/admin.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { RegisterUser } from 'src/app/common/interfaces/register-user';


@Component({
  selector: 'app-manage-admin',
  templateUrl: './manage-admin.component.html',
  styleUrls: ['./manage-admin.component.css']
})
export class ManageAdminComponent implements OnInit {

  @Input()
  admins: Admin[];

  @Output()
  emitRegisterAdmin = new EventEmitter<RegisterUser>();

  @Output()
  emitDeleteAdmin = new EventEmitter<string>();

  private p = 1;

  constructor(
    private readonly notificator: NotificatorService,
    private readonly adminService: AdminService,
  ) { }

  ngOnInit() {
  }

  registerUser(regUser: RegisterUser) {
    this.emitRegisterAdmin.emit(regUser);
  }

  adminToDelete(adminId: string) {
    this.emitDeleteAdmin.emit(adminId);
  }

}
