import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { RedemptionRecord } from 'src/app/common/interfaces/redemption-record';
import { RedemptionRecordService } from 'src/app/core/services/redemption-record.service';

@Injectable({
  providedIn: 'root'
})
export class RedemptionRecordResolver
  implements Resolve<RedemptionRecord[] | {}> {
  constructor(
    private readonly redemtionRecordService: RedemptionRecordService
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.redemtionRecordService.getAllRedemptionRecord().pipe(
      catchError(res => {
        return of([]);
      })
    );
  }
}
