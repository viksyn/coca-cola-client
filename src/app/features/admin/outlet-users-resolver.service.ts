import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { User } from 'src/app/common/interfaces/user';
import { UserService } from 'src/app/core/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class OutletUsersResolverService implements Resolve<User[] | {}> {
  constructor(private readonly userService: UserService) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const customerId = route.paramMap.get('customerId');
    const outletId = route.paramMap.get('outletId');
    return this.userService.getAllUsersForOutlet(customerId, outletId).pipe(
      catchError(res => {
        return of([]);
      })
    );
  }
}
