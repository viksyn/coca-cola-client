import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Outlet } from 'src/app/common/interfaces/outlet';
import { OutletService } from 'src/app/core/services/outlet.service';

@Injectable({
  providedIn: 'root'
})
export class OutletResolverService implements Resolve<Outlet[] | {}> {
  constructor(private readonly outletService: OutletService) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const customerId = route.paramMap.get('customerId');
    return this.outletService.getCustomerOutlets(customerId).pipe(
      catchError(res => {
        return of([]);
      })
    );
  }
}
