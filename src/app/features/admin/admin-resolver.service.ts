import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AdminService } from 'src/app/core/services/admin.service';
import { Admin } from 'src/app/common/interfaces/admin';

@Injectable({
  providedIn: 'root'
})
export class AdminResolverService implements Resolve<Admin[] | {}> {
  constructor(private readonly adminService: AdminService) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.adminService.getAllAdmins().pipe(
      catchError(res => {
        return of({ admins: [] });
      })
    );
  }
}
