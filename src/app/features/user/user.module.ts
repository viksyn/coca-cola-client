import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { TokenInterceptorService } from 'src/app/auth/token-interceptor.service';
import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { BarcodeScannerComponent } from './barcode-scanner/barcode-scanner.component';
import { ConfirmationPopUpComponent } from './barcode-scanner/confirmation-pop-up/confirmation-pop-up.component';
import { EnterBarcodeComponent } from './enter-barcode/enter-barcode.component';
import { EnterPrizeBarcodeComponent } from './enter-barcode/enter-prize-barcode/enter-prize-barcode.component';
import { PrizeBarcodeScannerComponent } from './prize-barcode-scanner/prize-barcode-scanner.component';
import { WrongPrizeBarcodeModalComponent } from './prize-barcode-scanner/wrong-prize-barcode-modal/wrong-prize-barcode-modal.component';
import { OutletRedemptionsComponent } from './user-history/outlet-redemptions/outlet-redemptions.component';
import { UserHistoryComponent } from './user-history/user-history.component';
import { UserRedemptionsComponent } from './user-history/user-redemptions/user-redemptions.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    UserComponent,
    BarcodeScannerComponent,
    UserProfileComponent,
    UserHistoryComponent,
    UserRedemptionsComponent,
    OutletRedemptionsComponent,
    ConfirmationPopUpComponent,
    EnterBarcodeComponent,
    EnterPrizeBarcodeComponent,
    PrizeBarcodeScannerComponent,
    WrongPrizeBarcodeModalComponent
  ],
  entryComponents: [
    ConfirmationPopUpComponent,
    WrongPrizeBarcodeModalComponent
  ],
  imports: [
    CoreModule,
    UserRoutingModule,
    NgbModule,
    SharedModule,
    ZXingScannerModule,
    NgxPaginationModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ]
})
export class UserModule { }
