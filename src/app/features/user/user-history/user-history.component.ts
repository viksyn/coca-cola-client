import { Component, OnInit } from '@angular/core';
import { RedemptionRecord } from 'src/app/common/interfaces/redemption-record';
import { UserService } from 'src/app/core/services/user.service';
import { StorageService } from 'src/app/core/services/storage.service';

@Component({
  selector: 'app-user-history',
  templateUrl: './user-history.component.html',
  styleUrls: ['./user-history.component.css']
})
export class UserHistoryComponent implements OnInit {

  username: string;
  outletRedemptions: RedemptionRecord[];
  userRedemptions: RedemptionRecord[];

  constructor(
    private readonly userService: UserService,
    private readonly storage: StorageService,
  ) { }

  ngOnInit() {

    this.userService.getUsersRedemptionRecord().subscribe(data => {
      this.username = this.storage.get('username');
      this.outletRedemptions = data;
      this.userRedemptions = this.outletRedemptions.filter(username => username.user.userName === this.username);
    });
  }
}


