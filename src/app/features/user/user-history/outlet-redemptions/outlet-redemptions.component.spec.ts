import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutletRedemptionsComponent } from './outlet-redemptions.component';

describe('OutletRedemptionsComponent', () => {
  let component: OutletRedemptionsComponent;
  let fixture: ComponentFixture<OutletRedemptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutletRedemptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutletRedemptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
