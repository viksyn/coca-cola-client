import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-outlet-redemptions',
  templateUrl: './outlet-redemptions.component.html',
  styleUrls: ['./outlet-redemptions.component.css']
})
export class OutletRedemptionsComponent implements OnInit {

  @Input()
  outletRedemptions: [];

  private p = 1;

  constructor() { }

  ngOnInit() {
  }

}
