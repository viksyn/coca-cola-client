import { Component, OnInit, Input } from '@angular/core';
import { RedemptionRecord } from 'src/app/common/interfaces/redemption-record';

@Component({
  selector: 'app-user-redemptions',
  templateUrl: './user-redemptions.component.html',
  styleUrls: ['./user-redemptions.component.css']
})
export class UserRedemptionsComponent implements OnInit {

  @Input()
  username: string;

  @Input()
  userRedemptions: [];

  constructor() { }

  ngOnInit() {
  }

}
