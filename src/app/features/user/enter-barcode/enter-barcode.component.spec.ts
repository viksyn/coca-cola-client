import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterBarcodeComponent } from './enter-barcode.component';

describe('EnterBarcodeComponent', () => {
  let component: EnterBarcodeComponent;
  let fixture: ComponentFixture<EnterBarcodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterBarcodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterBarcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
