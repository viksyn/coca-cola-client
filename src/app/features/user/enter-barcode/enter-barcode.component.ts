import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BarcodeService } from 'src/app/core/services/barcode.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { ConfirmationPopUpComponent } from '../barcode-scanner/confirmation-pop-up/confirmation-pop-up.component';

@Component({
  selector: 'app-enter-barcode',
  templateUrl: './enter-barcode.component.html',
  styleUrls: ['./enter-barcode.component.css']
})
export class EnterBarcodeComponent implements OnInit {
  redemptionConfirmed = false;
  winningBarcode: string;
  inputBarcodeToDelete: string;
  constructor(
    private readonly barcodeService: BarcodeService,
    private readonly modalService: NgbModal,
    private readonly notificator: NotificatorService,
    private readonly router: Router
  ) { }

  ngOnInit() { }

  submitBarcode(barcode: string) {
    this.barcodeService.submitBarcode(barcode).subscribe(
      validity => {
        this.openModal(validity.message, barcode);
      },
      error => {
        this.notificator.error('Invalid barcode!');
      }
    );
  }

  openModal(status: string, barcode: string) {
    const modalRef = this.modalService.open(ConfirmationPopUpComponent);
    modalRef.componentInstance.barcodeStatus = status;

    modalRef.result.then(result => {
      if (result === 'confirm') {
        this.redemptionConfirmed = true;
        this.winningBarcode = barcode;

      } else if (result === 'cancel') {
        this.router.navigate(['user']);
      }
    });
  }

  redeemPrize(prizeBarcode: string) {
    this.barcodeService
      .redeemPrize(this.winningBarcode, prizeBarcode)
      .subscribe(
        redemptionRecord => {
          this.router.navigate(['user']);
          this.redemptionConfirmed = false;
          this.inputBarcodeToDelete = '';
        },
        error => {
          this.notificator.error('Prize Code is invalid');
          this.router.navigate(['user']);
        }
      );
  }
}
