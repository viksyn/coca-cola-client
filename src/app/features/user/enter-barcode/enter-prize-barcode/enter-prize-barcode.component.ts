import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-enter-prize-barcode',
  templateUrl: './enter-prize-barcode.component.html',
  styleUrls: ['./enter-prize-barcode.component.css']
})
export class EnterPrizeBarcodeComponent implements OnInit {
  @Output()
  emitPrizeBarcode = new EventEmitter<string>();

  inputPrizeBarcodeToDelete: string;

  constructor() {}

  ngOnInit() {}

  submitBarcode(prizeBarcode: string) {
    this.emitPrizeBarcode.emit(prizeBarcode);
    this.inputPrizeBarcodeToDelete = '';
  }
}
