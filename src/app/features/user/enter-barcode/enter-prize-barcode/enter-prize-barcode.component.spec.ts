import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterPrizeBarcodeComponent } from './enter-prize-barcode.component';

describe('EnterPrizeBarcodeComponent', () => {
  let component: EnterPrizeBarcodeComponent;
  let fixture: ComponentFixture<EnterPrizeBarcodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterPrizeBarcodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterPrizeBarcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
