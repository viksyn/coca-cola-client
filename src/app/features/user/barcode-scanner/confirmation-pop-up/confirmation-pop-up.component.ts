import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BarcodeStatus } from 'src/app/common/enum/barcode-status';

@Component({
  selector: 'app-confirmation-pop-up',
  templateUrl: './confirmation-pop-up.component.html',
  styleUrls: ['./confirmation-pop-up.component.css']
})
export class ConfirmationPopUpComponent implements OnInit {
  @Input()
  barcodeStatus: string;
  barcodeStatusEnum = BarcodeStatus;

  constructor(private readonly activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  confirmRedemption(response: string) {
    this.activeModal.close(response);
  }
}
