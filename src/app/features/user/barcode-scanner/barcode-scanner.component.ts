import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { BarcodeService } from 'src/app/core/services/barcode.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { ConfirmationPopUpComponent } from './confirmation-pop-up/confirmation-pop-up.component';

@Component({
  selector: 'app-barcode-scanner',
  templateUrl: './barcode-scanner.component.html',
  styleUrls: ['./barcode-scanner.component.css']
})
export class BarcodeScannerComponent implements OnInit {
  resultFromModal: Subscription;
  constructor(
    public readonly barcodeService: BarcodeService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly modalService: NgbModal
  ) {}

  ngOnInit(): void {}

  scanSuccessHandler(barcode: string) {
    this.barcodeService.submitBarcode(barcode).subscribe(barcodeValidity => {
      const modalRef = this.modalService.open(ConfirmationPopUpComponent);
      modalRef.componentInstance.barcodeStatus = barcodeValidity.message;

      modalRef.result.then(result => {
        if (result === 'confirm') {
          this.router.navigate(['user', 'barcode-scan', barcode]);
        } else if (result === 'cancel') {
          this.router.navigate(['user']);
        } else if (result === 'report') {
          this.barcodeService
            .reportMultipleRedemptionAttempt(barcode)
            .subscribe(
              response => {
                console.log(response);

                this.notificator.success('Successfully reported');
                this.router.navigate(['user']);
              },
              error => {
                console.log(error);
              }
            );
        }
      });
    });
  }
}
