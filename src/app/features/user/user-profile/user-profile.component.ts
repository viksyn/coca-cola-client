import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/common/interfaces/user';
import { StorageService } from 'src/app/core/services/storage.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  userId: string;
  username: string;
  user: User;
  brandName: string;
  outlateName: string;
  createdOn: Date;

  constructor(
    private readonly userService: UserService,
    private readonly route: ActivatedRoute,
    private readonly storage: StorageService
  ) {}

  ngOnInit() {
    this.userId = this.storage.get('userId');
    this.userService.getCurrentUser(this.userId).subscribe(data => {
      this.user = data;
      this.username = data.userName;
      this.brandName = data.brandName.brandName;
      this.outlateName = data.outletName.location;
      this.createdOn = data.createdOn;
    });
  }
}
