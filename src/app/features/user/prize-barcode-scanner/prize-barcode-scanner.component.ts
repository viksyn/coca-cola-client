import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BarcodeService } from 'src/app/core/services/barcode.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { WrongPrizeBarcodeModalComponent } from './wrong-prize-barcode-modal/wrong-prize-barcode-modal.component';

@Component({
  selector: 'app-prize-barcode-scanner',
  templateUrl: './prize-barcode-scanner.component.html',
  styleUrls: ['./prize-barcode-scanner.component.css']
})
export class PrizeBarcodeScannerComponent implements OnInit {
  barcode: string;
  constructor(
    public readonly barcodeService: BarcodeService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly modalService: NgbModal
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(barcode => {
      this.barcode = barcode.get('barcode');
    });
  }

  scanPrizeBarcodeHandler(prizeBarcode: string) {

    this.barcodeService.redeemPrize(this.barcode, prizeBarcode).subscribe(
      redemptionRecord => {
        this.notificator.success(`Successfully redeemed`);
        this.router.navigate(['user']);
      },
      error => {
        console.log(error);

        const modalRef = this.modalService.open(
          WrongPrizeBarcodeModalComponent
        );
        modalRef.componentInstance.barcodeStatus = error.message;
        modalRef.result.then(result => {
          if (result === 'cancel') {
            this.router.navigate(['user']);
          }
        });
      }
    );
  }
}
