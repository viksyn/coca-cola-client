import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-wrong-prize-barcode-modal',
  templateUrl: './wrong-prize-barcode-modal.component.html',
  styleUrls: ['./wrong-prize-barcode-modal.component.css']
})
export class WrongPrizeBarcodeModalComponent implements OnInit {
  @Input()
  barcodeStatus: string;
  constructor(private readonly activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  chosenOption(response: string) {
    this.activeModal.close(response);
  }
}
