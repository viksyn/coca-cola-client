import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WrongPrizeBarcodeModalComponent } from './wrong-prize-barcode-modal.component';

describe('WrongPrizeBarcodeModalComponent', () => {
  let component: WrongPrizeBarcodeModalComponent;
  let fixture: ComponentFixture<WrongPrizeBarcodeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WrongPrizeBarcodeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrongPrizeBarcodeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
