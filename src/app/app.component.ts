import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from './core/services/auth.service';
import { BarcodeService } from './core/services/barcode.service';
import { NotificatorService } from './core/services/notificator.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  public username = '';
  public role = '';
  public isLogged = false;
  private subscription: Subscription;

  constructor(
    private readonly notificator: NotificatorService,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly barcodeServce: BarcodeService
  ) {}

  search(searchedTextValue: string) {}

  logout() {
    this.authService.logout();
    this.router.navigate(['login']);
    this.notificator.success(`Successful logout.`);
  }

  ngOnInit() {
    this.subscription = this.authService.user$.subscribe(obsUsername => {
      if (obsUsername === null) {
        this.username = '';
        this.isLogged = false;
        this.router.navigate(['login']);
      } else {
        this.authService.roles$.subscribe(role => {
          if (role) {
            this.role = role[0];
          }
        });
        this.username = obsUsername;
        this.isLogged = true;
      }
    });
  }

  submitIssue(issueObj: { name: string; email: string; issue: string }) {
    return this.barcodeServce.submitIssue(issueObj).subscribe();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
