import { ActivityType } from '../enum/activity-type';
import { FeatureType } from '../enum/feature-type';

export interface Activity {
  id: string;

  modifyingUserId: string;

  modifyingUserName: string;

  featureType: FeatureType;

  actionType: ActivityType;

  itemId: string;

  itemName: string;

  createdOn: Date;

  updatedOn: Date;

  version: number;
}
