export interface User {

  id?: string;
  userName?: string;
  email?: string;
  createdOn?: Date;
  roles?: string;
  brandName?: {
    id: string;
    brandName: string;
  };
  outletName?: {
    id: string;
    location: string;
  };

}
