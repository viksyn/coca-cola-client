import { PrizeType } from '../enum/prize-type';

export interface Prize {
  barcode: string;
  prize: PrizeType;
  count: number;
  createdOn: Date;
  updatedOn: Date;
  version: number;
}
