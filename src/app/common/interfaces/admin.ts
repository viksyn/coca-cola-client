export interface Admin {
  id: string;
  userName: string;
  createdOn: Date;
  roles: string;
}
