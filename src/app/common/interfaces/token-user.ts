export interface TokenUser {
  email: string;
  name: string;
  id: string;
  roles: {
    id: string;
    name: string;
  }[];
  iat: number;
  exp: number;
}
