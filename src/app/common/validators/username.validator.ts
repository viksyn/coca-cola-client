import { ValidatorFn, AbstractControl } from '@angular/forms';

export function ForbiddenUsernameValidator(username: string): ValidatorFn {
  return (control: AbstractControl) => {
    const forbidden = control.value === username;
    return forbidden ? { forbiddenUsername: { value: control.value } } : null;
  };
}
