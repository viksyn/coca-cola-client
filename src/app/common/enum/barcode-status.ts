export enum BarcodeStatus {
  VALID = 'Valid!',
  INVALID = 'Invalid!',
  REDEEMED = 'Already redeemed'
}
