export enum FeatureType {
  customer = 'customer',
  outlet = 'outlet',
  user = 'user'
}
