'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">coca-cola-client documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AdminModule.html" data-type="entity-link">AdminModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AdminModule-353758606eb72b0f3f490e9fb253fa80"' : 'data-target="#xs-components-links-module-AdminModule-353758606eb72b0f3f490e9fb253fa80"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AdminModule-353758606eb72b0f3f490e9fb253fa80"' :
                                            'id="xs-components-links-module-AdminModule-353758606eb72b0f3f490e9fb253fa80"' }>
                                            <li class="link">
                                                <a href="components/AdminComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AdminComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AuditTrailComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuditTrailComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ConfirmationComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConfirmationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CreateCustomerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CreateCustomerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DisplayOutletUsersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DisplayOutletUsersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditCustomerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EditCustomerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditUserComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EditUserComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ManageAdminComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ManageAdminComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ManageAllUsersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ManageAllUsersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ManageCustomersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ManageCustomersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ManageOutletUsersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ManageOutletUsersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ManageOutletsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ManageOutletsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ManageSingleCustomerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ManageSingleCustomerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ManageUsersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ManageUsersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RedemptionRecordComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RedemptionRecordComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RegisterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RegisterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ReportsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ReportsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AdminModule-353758606eb72b0f3f490e9fb253fa80"' : 'data-target="#xs-injectables-links-module-AdminModule-353758606eb72b0f3f490e9fb253fa80"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AdminModule-353758606eb72b0f3f490e9fb253fa80"' :
                                        'id="xs-injectables-links-module-AdminModule-353758606eb72b0f3f490e9fb253fa80"' }>
                                        <li class="link">
                                            <a href="injectables/CustomerService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>CustomerService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AdminRoutingModule.html" data-type="entity-link">AdminRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-ea530e7a17826e29df155aa909b2ef55"' : 'data-target="#xs-components-links-module-AppModule-ea530e7a17826e29df155aa909b2ef55"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-ea530e7a17826e29df155aa909b2ef55"' :
                                            'id="xs-components-links-module-AppModule-ea530e7a17826e29df155aa909b2ef55"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FooterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/IssueComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">IssueComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LoginComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavbarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NavbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link">CoreModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CoreModule-ced9c54247135cfea165241e4bcd6aac"' : 'data-target="#xs-injectables-links-module-CoreModule-ced9c54247135cfea165241e4bcd6aac"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CoreModule-ced9c54247135cfea165241e4bcd6aac"' :
                                        'id="xs-injectables-links-module-CoreModule-ced9c54247135cfea165241e4bcd6aac"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/JwtService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>JwtService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/NotificatorService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>NotificatorService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/StorageService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>StorageService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link">SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-5caee22fd07145e9e65610ff2bf96896"' : 'data-target="#xs-components-links-module-SharedModule-5caee22fd07145e9e65610ff2bf96896"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-5caee22fd07145e9e65610ff2bf96896"' :
                                            'id="xs-components-links-module-SharedModule-5caee22fd07145e9e65610ff2bf96896"' }>
                                            <li class="link">
                                                <a href="components/NotFoundComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotFoundComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link">UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UserModule-f76f0f7b3384ec6f1303b70b512d2c2d"' : 'data-target="#xs-components-links-module-UserModule-f76f0f7b3384ec6f1303b70b512d2c2d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UserModule-f76f0f7b3384ec6f1303b70b512d2c2d"' :
                                            'id="xs-components-links-module-UserModule-f76f0f7b3384ec6f1303b70b512d2c2d"' }>
                                            <li class="link">
                                                <a href="components/BarcodeScannerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BarcodeScannerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ConfirmationPopUpComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConfirmationPopUpComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EnterBarcodeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EnterBarcodeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EnterPrizeBarcodeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EnterPrizeBarcodeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/OutletRedemptionsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">OutletRedemptionsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PrizeBarcodeScannerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PrizeBarcodeScannerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserHistoryComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserHistoryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserProfileComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserProfileComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserRedemptionsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserRedemptionsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WrongPrizeBarcodeModalComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">WrongPrizeBarcodeModalComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserRoutingModule.html" data-type="entity-link">UserRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AppPage.html" data-type="entity-link">AppPage</a>
                            </li>
                            <li class="link">
                                <a href="classes/BarcodeDTO.html" data-type="entity-link">BarcodeDTO</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AdminService.html" data-type="entity-link">AdminService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link">AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BarcodeService.html" data-type="entity-link">BarcodeService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CustomerService.html" data-type="entity-link">CustomerService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtService.html" data-type="entity-link">JwtService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NotificatorService.html" data-type="entity-link">NotificatorService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/OutletService.html" data-type="entity-link">OutletService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RedemptionRecordService.html" data-type="entity-link">RedemptionRecordService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StorageService.html" data-type="entity-link">StorageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link">UserService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/TokenInterceptorService.html" data-type="entity-link">TokenInterceptorService</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/ActivityResolverService.html" data-type="entity-link">ActivityResolverService</a>
                            </li>
                            <li class="link">
                                <a href="guards/AdminGuard.html" data-type="entity-link">AdminGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/AdminResolverService.html" data-type="entity-link">AdminResolverService</a>
                            </li>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link">AuthGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/CustomerResolveService.html" data-type="entity-link">CustomerResolveService</a>
                            </li>
                            <li class="link">
                                <a href="guards/OutletResolverService.html" data-type="entity-link">OutletResolverService</a>
                            </li>
                            <li class="link">
                                <a href="guards/OutletUsersResolverService.html" data-type="entity-link">OutletUsersResolverService</a>
                            </li>
                            <li class="link">
                                <a href="guards/RedemptionRecordResolver.html" data-type="entity-link">RedemptionRecordResolver</a>
                            </li>
                            <li class="link">
                                <a href="guards/UserGuard.html" data-type="entity-link">UserGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/UserResolverService.html" data-type="entity-link">UserResolverService</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Activity.html" data-type="entity-link">Activity</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Admin.html" data-type="entity-link">Admin</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Customer.html" data-type="entity-link">Customer</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Outlet.html" data-type="entity-link">Outlet</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Prize.html" data-type="entity-link">Prize</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/RedemptionRecord.html" data-type="entity-link">RedemptionRecord</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/RegisterUser.html" data-type="entity-link">RegisterUser</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TokenUser.html" data-type="entity-link">TokenUser</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/User.html" data-type="entity-link">User</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});