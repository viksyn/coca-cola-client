# CocaColaClient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.
User Manual: https://drive.google.com/file/d/1oLKWEbBceB8JgeGG7bsF4hEB30tylL0N/view?usp=sharing

## Development server

1. Run `npm i` command.
2. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Documentation

For initial AngularCLI documentation, run `compodoc -p tsconfig.json -s` or `npx compodoc -s` and open your browser and navigate to `http://127.0.0.1:8080`;

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
